// console.log("hi");

/*
	While loop - takes a single condition. If the condition is true it will run the code.

		Syntax:
			while(condition) {
				statement
			}
*/

let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--;
}

let num = 0;

while(num <= 5){
	console.log("While: " + num);
	num++;
}

/*
	The While loop should only display even number and increment by 2.

	let numA = 0;
	while(numA = 30) {
	console.log("While: " + +numA);
	numA ++2;
	}
*/

let evenNum = 0;

while(evenNum <= 10){
	num = evenNum % 2;
	if (num === 0){
		console.log("While: " + evenNum)
	} 
	evenNum +=2;
}

let numA = 0;
while(numA <= 30) {
	console.log("While: " + numA);
	numA +=2;
	}

/*
	Do While Loop
		- a do while loop works a lot like the while loop. But unlike while loop, do while loop guarantee that the code will be executed at least once.

	Syntax:
		do {
			statement
		} while (expression/condition)
*/

/*let number = Number(prompt("Give me a number"));

do {
	console.log("Do while: " + number);
	number++
} while (number < 10);*/

/*number = Number(prompt("Give me another number"));

do {
	console.log("Do while: " + number);
	number--
} while (number > 10);*/


/*
	For loop
		- is more flexible than the while loop and do while lop
		
		- part:
			- initial value: tracks the progress of the loop
			- condition: if true it will run the code; if false it will stop the iteration/code
			- iteration: It indicates how to advance the loop (increasing or decreasing); final expression.
	Syntax:
		for(initialValue; condition; iteration)
*/

for(let count = 0; count <= 20; count++) {
	console.log("For Loop: " + count);
}

let myString = "jean eraya";
console.log(myString.length);

console.log(" ");
console.log(myString[9]);
console.log(" ");

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "JEAN VICTORIA";

for (let n = 0; n < myName.length; n++){
	if(
		myName[n].toLowerCase() == "a" || 
		myName[n].toLowerCase() == "e" ||
		myName[n].toLowerCase() == "i" ||
		myName[n].toLowerCase() == "o" ||
		myName[n].toLowerCase() == "u"
	){
		console.log("Vowel");
	}else{
		console.log(myName[n].toLowerCase());
	}
};

let firstString = "extravagant";
console.log(" ");
console.log("The word of the day is " + firstString);
let consonant = " ";

for (let a = 0; a < firstString.length; a++){
	if(
		firstString[a] === "a" ||
		firstString[a] === "e" ||
		firstString[a] === "i" ||
		firstString[a] === "o" ||
		firstString[a] === "u"
	){
		continue;
	}else{
		consonant += firstString[a];
		
	}
}
console.log(consonant);

// Continue and Break Statement

/*
	"continue" statement allows the code to go to the next iteration without finishing the execution of all the statements in the code block.

	"break" statement on the other hand is keyword that ends the execution of the code or the current loop
*/

console.log(" ");
for (let count = 1; count <= 20; count++){
	if ( count % 5 === 0){
		console.log("Divisible by 5")
		continue;
	}
	console.log("continue and break: " + count)
	if(count > 10) {
		break;
	}
}

console.log(" ");
let name = "Alexander";

for (let i = 0; i < name.length; i++){
	console.log(name[i])

	if(name[i].toLowerCase() === "a"){
		console.log("Continue iteration.");
		continue;
	}
	if(name[i].toLowerCase() === "d"){
		console.log("break");
		break;
	}
}