/*console.log("hi");*/

let num = parseInt(prompt("Enter a number that's greater than 100"));
console.log("The number you provided is: " + num);

for (let a = 0; num >= a; num--){
	//console.log(num[a]);
	if(num <= 50){
		console.log("Current number is: " + num);
		break;
	}
	let isDivBy10 = num % 10;
	if(isDivBy10 === 0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}
	let isDivBy5 = num % 5;
	if (isDivBy5 === 0) {
		console.log(num);
		continue;
	}
}

console.log(" ");

let myString = "supercalifragilisticexpialidocious";
console.log(myString);
let consonants = " ";
//console.log(consonants)

for (a = 0; a < myString.length; a++) {

	if(
		myString[a].toLowerCase() == 'a' ||
		myString[a].toLowerCase() == 'e' ||
		myString[a].toLowerCase() == 'i' ||
		myString[a].toLowerCase() == 'o' ||
		myString[a].toLowerCase() == 'u' 
	) {
		continue;

	} else {
		consonants += myString[a];
	}
}
console.log(consonants);